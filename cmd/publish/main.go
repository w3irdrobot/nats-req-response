package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/nats-io/nats.go"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	natsreqresponse "gitlab.com/searsaw/nats-req-response"
	"gitlab.com/searsaw/nats-req-response/pkg/utils"
)

var (
	MaxReconnectAttempts = 5
)

type httpMessage struct {
	Number int `json:"number"`
}

func main() {
	natsUrl := flag.String("nats-url", nats.DefaultURL, "url of the NATS server")
	port := flag.String("port", "8080", "port of the HTTP server")
	flag.Parse()

	conn, err := utils.TryNatsConnect(*natsUrl, "number-sender", MaxReconnectAttempts)
	if err != nil {
		fmt.Printf("error connecting to NATS: %s\n", err)
		os.Exit(1)
	}
	fmt.Println("connected to NATS")
	defer conn.Close()

	encodedConnection, err := nats.NewEncodedConn(conn, nats.JSON_ENCODER)
	if err != nil {
		fmt.Printf("error creating the JSON encoded conneciton: %s\n", err)
		os.Exit(1)
	}

	http.HandleFunc("/multiply", sendToNats(encodedConnection))
	http.Handle("/metrics", promhttp.Handler())

	fmt.Printf("the server is running on :%s\n", *port)
	if err := http.ListenAndServe(":"+*port, nil); err != http.ErrServerClosed {
		fmt.Printf("error closing down the server: %s\n", err)
		os.Exit(1)
	}
	fmt.Println("the server has shutdown successfully")
}

func sendToNats(conn *nats.EncodedConn) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		var body httpMessage
		if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
			fmt.Printf("error decoding the payload: %s\n", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		fmt.Printf("sending request to multiply the number %d\n", body.Number)
		msg := natsreqresponse.Message{Data: natsreqresponse.Data{Number: body.Number}}
		var reply natsreqresponse.Message
		err := conn.Request("multiply", msg, &reply, 2*time.Second)
		if conn.LastError() != nil {
			fmt.Printf("error sending the request to NATS: %s\n", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		if err != nil {
			fmt.Printf("error sending the request to NATS: %s\n", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		if msg.Status == natsreqresponse.Error {
			fmt.Printf("error processing the request: %s\n", msg.Data.Error)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		response := httpMessage{Number: reply.Data.Number}
		if err := json.NewEncoder(w).Encode(response); err != nil {
			fmt.Printf("error sending the request to NATS: %s\n", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
}
