.PHONY: start
start:
	@docker-compose up --detach --scale pub=5 --scale sub=10

.PHONY: build
build:
	@docker-compose build

.PHONY: stop
stop:
	@docker-compose stop
