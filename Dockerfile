FROM golang:1.13 AS build
ARG app
WORKDIR /workspace
COPY . .
RUN GOOS=linux CGO_ENABLED=0 go build -v -o app ./cmd/${app}

FROM scratch
EXPOSE 8080
WORKDIR /workspace
COPY --from=build /workspace/app .
ENTRYPOINT ["./app"]
