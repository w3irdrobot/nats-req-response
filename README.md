NATS Request/Response Example
=============================

This is a simple example showing how to setup an HTTP server that accepts a JSON payload containing this number. That number is then published to a NATS server. On the other side is a set of subscribers that accept the number, multiply it by a number between one and twenty, and return the new number. This new number is then sent back to the original requester.

## Running

To run the example, you'll need `docker-compose` installed. Then run the following:

```shell
make start
```

Then to send requests using [`httpie`](https://httpie.org/), run the following:

```shell
http :8080/multiply number:=5
```
