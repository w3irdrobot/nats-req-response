module gitlab.com/searsaw/nats-req-response

go 1.13

require (
	github.com/nats-io/nats-server/v2 v2.1.2 // indirect
	github.com/nats-io/nats.go v1.9.1
	github.com/prometheus/client_golang v1.4.0
)
